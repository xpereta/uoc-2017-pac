//
//  NotesViewController.swift
//  PAC
//
//  Created by Xavier Pereta on 26/02/2017.
//  Copyright © 2017 uoc. All rights reserved.
//

import UIKit

class NotesViewController: UITableViewController {

    var notes = Array<String>()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let statusBarHeight = UIApplication.shared.statusBarFrame.height
        
        let insets = UIEdgeInsets(top: statusBarHeight, left: 0, bottom: 0, right: 0)
        tableView.contentInset = insets
        tableView.scrollIndicatorInsets = insets
        
        loadNotes()
    }
    
    func loadNotes() {
        for i in 1...100 {
            notes.append("Note \(i)")
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Note")
        cell?.textLabel?.text = notes[row]
        
        return cell!
    }
}
